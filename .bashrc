#!/bin/bash
######################################
###### BIG  SETTINGS  STUFF ##########
######################################

stty -ixon # Disable ctrl-s and ctrl-q
set -o vi
bind -m vi-insert "\C-l":clear-screen
export HISTCONTROL=ignoreboth:erasedups
PS1='\u@\W \$ '

# If not running interactively, don't do anything
[[ $- != *i* ]] && return

if [ -d "$HOME/.bin" ] ;
  then PATH="$HOME/.bin:$PATH"
fi

if [ -d "$HOME/.local/bin" ] ;
  then PATH="$HOME/.local/bin:$PATH"
fi

if [ -d "$HOME/.scripts" ] ;
  then PATH="$HOME/.scripts:$PATH"
fi

#shopt
shopt -s autocd # change to named directory
shopt -s cmdhist # save multi-line commands in history as single line
shopt -s dotglob # include . files in filename expansions
shopt -s histappend # do not overwrite history
shopt -s expand_aliases # expand aliases

######################################
###### CLEAN UP ######################
######################################


######################################
###### SOME  ALIASES #################
######################################
# Files Systems
alias ls='exa -al --color=always --group-directories-first'
alias lstree='exa -aT --color=always --group-directories-first'
alias df='df -h'
# Devouring
#alias zathura='devour zathura'
#alias mpv='devour mpv'
#alias sxiv='devour sxiv'
# Text
alias vim='nvim'
alias v='nvim'
# Coding
alias gccs='gcc -Wall -pedantic'
alias gccs99='gcc -Wall -pedantic -std=c99'
alias wwulinuxpool='ssh -p 922 -Y langed@linux-07.cs.wwu.edu'
# Git bare dotfiles stuff
alias config='/usr/bin/git --git-dir=$HOME/dotfiles/ --work-tree=$HOME'

pfetch
source /usr/share/nvm/init-nvm.sh
