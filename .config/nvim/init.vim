syntax on
let mapleader =" "
set encoding=utf-8
set nocompatible
filetype plugin on
set list

" Indentation
set tabstop=4 softtabstop=0 expandtab shiftwidth=4 smarttab

" Searching
set smartcase

" Backups
set noswapfile
set nobackup
set undodir=~/.config/nvim/undodir
set undofile

" Right column at 80 lines for good coding practice.
set colorcolumn=80

" QoL
set showmatch " Show matching Brackets
set number relativenumber " Side numbers

" Fuzzy finding by allowing searching into subfolders
set path+=**
set wildmenu
" use :find to find, and * to make it fuzzy.
" Also make use of :b.

" Delete trailing white space and newlines at end of file on save.
autocmd BufWritePre * %s/\s\+$//e
autocmd BufWritePre * %s/\n\+\%$//e

" Easy copy and pasting to external programs
nnoremap ,, :w<CR>:!gcpdf %<CR><CR>
map <C-y> "+yy
map <C-p> "+P

call plug#begin("~/.config/nvim/plugged")

" Core
Plug 'dracula/vim' " Dracula Theme
Plug 'tpope/vim-fugitive' " Git Plugin
Plug 'neoclide/coc.nvim' " Auto completion
Plug 'jremmen/vim-ripgrep' " grep inside of vim
Plug 'ap/vim-css-color' " Preview colors
Plug 'reedes/vim-pencil'

" As needed
"Plug 'pangloss/vim-javascript' " Javascript highlighting
"Plug 'evanleck/vim-svelte', {'branch': 'main'} " Svelte highlighting
"Plug 'mxw/vim-jsx' " JSX highlighting

call plug#end()

" Dracula Color Scheme
colorscheme dracula " Set it

" Use Terminal Transparency
hi Normal guibg=NONE ctermbg=NONE

" Rip Grep
if executable('rg')
  let g:rg_derive_root='true'
endif

" Language Specifics
" Easy compile java in vim
autocmd FileType java set makeprg=javac\ %
set errorformat=%A%f:%l:\ %m,%-Z%p^,%-C.%#
